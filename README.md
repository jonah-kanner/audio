# Audio Files 


# Audio files for confident detections in GWTC-1

These audio files were produced by the code in [audio.ipynb](./audio.ipynb)

All files and code are intended for educational purposes, and are not representative of 
published scientific results.

Template waveforms are based on the median values of 1-D marginalized parameters, and so do not 
match the data to a precision appropriate for scientific analysis.  

For each event, the strain data are real interferometer data that has been whitened and band-passed.
The template waveforms are given without detector noise and after applying a band-pass filter.  The 
template and data audio files have the mergers at approximately the same time, around 2 seconds before
the end of each file.  A glitch was removed from the L1, GW170817 data.

These data represent the very earliest detections of graviational wave signals, and in most cases, 
the signal-to-noise ratio (SNR) is very low.  In many cases, this makes the signals difficult or impossible to 
hear for all but the highest SNR signals.  With good audio equipment, both GW150914 and GW170817 can 
be heard in the data by most people.  Note that, as detector sensitivity improves with time, it is likely 
that signals will be more clearly audible in the data in future runs.  

Higher mass mergers, near a total mass of 50 solar masses, are 'in-band' for only a very short time before
merger, and sound as short 'thuds' lasting around a tenth of a second.  On the other hand, binary neutron 
star mergers are in-band for tens of seconds, and make a distinctive 'chirping' sound.  

All strain data have been downloaded from the Gravitational Wave Open Science Center at gw-openscience.org

## Table of Audio Files
Follow links to audio files in the table below

| Name | SNR | Total Mass | Template | Data | 
|---- | ---- | ---- | ---- | ---- | 
| GW150914 | 24.4 | 66.2 M<sub>&#9737;</sub> | [Waveform](wav/GW150914-template.wav) | [H1](wav/H1-GW150914-data.wav)  [L1](wav/L1-GW150914-data.wav) |
| GW151012 | 10.0 | 36.9 M<sub>&#9737;</sub> | [Waveform](wav/GW151012-template.wav) | [H1](wav/H1-GW151012-data.wav)  [L1](wav/L1-GW151012-data.wav) |
| GW151226 | 13.1 | 21.4 M<sub>&#9737;</sub> | [Waveform](wav/GW151226-template.wav) | [H1](wav/H1-GW151226-data.wav)  [L1](wav/L1-GW151226-data.wav) |
| GW170104 | 13.0 | 51.1 M<sub>&#9737;</sub> | [Waveform](wav/GW170104-template.wav) | [H1](wav/H1-GW170104-data.wav)  [L1](wav/L1-GW170104-data.wav) |
| GW170608 | 14.9 | 18.5 M<sub>&#9737;</sub> | [Waveform](wav/GW170608-template.wav) | [H1](wav/H1-GW170608-data.wav)  [L1](wav/L1-GW170608-data.wav) |
| GW170729 | 10.8 | 84.9 M<sub>&#9737;</sub> | [Waveform](wav/GW170729-template.wav) | [H1](wav/H1-GW170729-data.wav)  [L1](wav/L1-GW170729-data.wav)  [V1](wav/V1-GW170729-data.wav) |
| GW170809 | 12.4 | 59.0 M<sub>&#9737;</sub> | [Waveform](wav/GW170809-template.wav) | [H1](wav/H1-GW170809-data.wav)  [L1](wav/L1-GW170809-data.wav)  [V1](wav/V1-GW170809-data.wav) |
| GW170814 | 15.9 | 56.0 M<sub>&#9737;</sub> | [Waveform](wav/GW170814-template.wav) | [H1](wav/H1-GW170814-data.wav)  [L1](wav/L1-GW170814-data.wav)  [V1](wav/V1-GW170814-data.wav) |
| GW170817 | 33.0 | 2.73 M<sub>&#9737;</sub> | [Waveform](wav/GW170817-template.wav) | [H1](wav/H1-GW170817-data.wav)  [L1](wav/L1-GW170817-data.wav)  [V1](wav/V1-GW170817-data.wav)  [G1](wav/G1-GW170817-data.wav) |
| GW170818 | 11.3 | 62.3 M<sub>&#9737;</sub> | [Waveform](wav/GW170818-template.wav) | [H1](wav/H1-GW170818-data.wav)  [L1](wav/L1-GW170818-data.wav)  [V1](wav/V1-GW170818-data.wav) |
| GW170823 | 11.5 | 69.0 M<sub>&#9737;</sub> | [Waveform](wav/GW170823-template.wav) | [H1](wav/H1-GW170823-data.wav)  [L1](wav/L1-GW170823-data.wav) |
